(* ::Package:: *)

(* ::Title:: *)
(*Lecture 9: Boolean Algebra*)


(* Clear variables. *)
ClearAll["Global`*"]
(* Limit variable scope to just this document. *)
SetOptions[EvaluationNotebook[], CellContext -> Notebook]


(* ::Section:: *)
(*Notes*)


(* ::Text:: *)
(*Complement: \!\(\*OverscriptBox[\(X\), \(_\)]\)={x|x\[NotElement]X}}*)


(* ::Section:: *)
(*Daily problems*)


(* ::Subsection:: *)
(*8*)


(* https://reference.wolfram.com/language/guide/BooleanComputation.html *)


(* a *)
(*expr = Xor[Xor[x,y],x]*)


(* c *)
expr = (x&&y&&z)||(x&&y&&!z)||(x&&!y&&z)||(!x&&y&&z)


(* ::Subsubsection:: *)
(*Logical Circuit*)


bf = BooleanConvert[expr,"BooleanFunction"]


expr//ToString;
StringForm["Logic circuit ``",%]//ToString
WolframAlpha[%,IncludePods->{"LogicCircuit"}]


(* ::Subsubsection:: *)
(*Venn Diagram*)


(* http://mathematica.stackexchange.com/a/2558/39769 *)


expr//ToString;
StringForm["Venn diagram ``",%]//ToString
WolframAlpha[%,IncludePods->{"VennDiagram"}]


(* ::Subsubsection:: *)
(*Truth Table*)


BooleanTable[expr,{x,y}]//TableForm
