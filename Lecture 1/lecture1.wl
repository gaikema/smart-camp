(* ::Package:: *)

(* ::Title:: *)
(*Lecture 1*)


(* Clear variables. *)
ClearAll["Global`*"]
(* Limit variable scope to just this document. *)
SetOptions[EvaluationNotebook[], CellContext -> Notebook]


(* ::Section:: *)
(*Notes*)


ExtendedGCD[617,33]


(* ::Subsection:: *)
(*1.8: Euclid's algorithm*)


(* ::Text:: *)
(*Euclid's algortithm is a method for finding the GCD.*)
(*If a=bq+r, then gcd(a,b)=gcd(b,r).*)


(* ::Program:: *)
(*function gcd(a, b)*)
(*    while b != 0*)
(*       t := b; *)
(*       b := a mod b; *)
(*       a := t; *)
(*    return a;*)


(* ::Subsection:: *)
(*1.9*)


Clear[a,b]
(a^3-b^3)/(a-b)
Simplify[%]


(* ::Section:: *)
(*Problem set*)


(* ::Subsection:: *)
(*2*)


2^(300)<3^(200)


(* ::Subsection:: *)
(*21*)


(* http://www.wolframalpha.com/input/?i=1234%2F55+continued+fraction *)
ContinuedFraction[1234/55]


(* ::Subsection:: *)
(*22*)


FromContinuedFraction/@{{0,4,3},{2,7,8,3},{3,2,1,5}}


(* ::Subsection:: *)
(*35*)


Solve[x y+x+y==19 && x^2y+x y^2==84,{x,y}]
