(* ::Package:: *)

(* ::Title:: *)
(*Boolean Logic*)


(* ::Subtitle:: *)
(*Matt Gaikema*)


(* https://reference.wolfram.com/language/guide/LogicAndBooleanAlgebra.html *)


(* Clear variables. *)
ClearAll["Global`*"]
(* Limit variable scope to just this document. *)
SetOptions[EvaluationNotebook[], CellContext -> Notebook]
(* Set directory for images. *)
SetDirectory["C:\\Users\\mgaik\\Dropbox\\Math\\Smart camp\\SMaRT Camp\\Resources"]


(* ::Section:: *)
(*Logical definitions*)


(* ::Subsection:: *)
(*And*)


(* http://reference.wolfram.com/language/example/CreateTruthTables.html *)
expr = x&&y
bf = BooleanConvert[expr,"BooleanFunction"];
BooleanTable[{x,y}->bf,{x,y}]//TableForm


WolframAlpha[ToString[expr],Asynchronous->True]


(* ::Subsection::Closed:: *)
(*Or*)


expr = x||y


WolframAlpha[ToString[expr],Asynchronous->True]


(* ::Subsection::Closed:: *)
(*Xor*)


expr = Xor[x,y]


WolframAlpha[ToString[expr],Asynchronous->True]


(* ::Subsection::Closed:: *)
(*Coincidence*)


(* ::Text:: *)
(*x is equal to y.*)


expr = x==y


WolframAlpha[ToString[expr],Asynchronous->True]


(* ::Subsection::Closed:: *)
(*Nand*)


(* ::Text:: *)
(*At least one of x and y is false.*)


expr = Nand[x,y]


WolframAlpha[ToString[expr],Asynchronous->True]


(* ::Subsection::Closed:: *)
(*Nor*)


(* ::Text:: *)
(*Neither x nor y.*)


expr = Nor[x,y]


WolframAlpha[ToString[expr],Asynchronous->True]


(* ::Subsection::Closed:: *)
(*Implication*)


expr = Implies[x,y]


WolframAlpha[ToString[expr],Asynchronous->True]


(* ::Section:: *)
(*Universality and Toffoli gate*)


(* ::Text:: *)
(*Any reversible gate must have the same number of input and output bits, by the pigeonhole principle.*)
(*For one input bit, there are two possible reversible gates.*)
(*One of them is the NOT gate.*)
(*The other is the identity gate which maps its input to its output unchanged.*)
(**)
(*For two input bits, the only non-trivial gate is the reversible XOR gate which XORs the first bit *)
(*to the second bit and leaves the first bit unchanged.*)


reversibleXor = {{0,0}->{0,0},{0,1}->{0,1},{1,0}->{1,1},{1,1}->{1,0}};
BooleanFunction[reversibleXor]
reversibleXor//TableForm
TeXForm[TableForm[reversibleXor]]


(* ::Text:: *)
(*Unfortunately, there are reversible functions which cannot be computed using just those gates.*)
(*The set consisting of NOT and XOR gates is not universal.*)
(*If we need to compute an arbitrary function by reversible gates, we need another gate, the Toffoli gate.*)
(**)
(*The Toffoli gate as a 3-bit input and output. If the first 2 bits are set, it flips the third bit.*)


toffoli = {
	{0,0,0}->{0,0,0},
	{0,0,1}->{0,0,1},
	{0,1,0}->{0,1,0},
	{0,1,1}->{0,1,1},
	{1,0,0}->{1,0,0},
	{1,0,1}->{1,0,1},
	{1,1,0}->{1,1,1},
	{1,1,1}->{1,1,0}
};
BooleanFunction[toffoli]
toffoli//TableForm
TeXForm[TableForm[toffoli]]


(* ::Text:: *)
(*It can also be described as mapping bits a, b, and c to a, b, and c XOR (a and b).*)
(**)
(*The Toffoli gate is universal; this means that for any boolean function f(x1,x2,...,xm), there is a circuit consisting of Toffoli gates which*)
(*takes x1,x2,...,xm and some extra bits set to 0 or 1 and outputs x1,x2,...,xm,f(x1,x2,...,xm), and some garbage bits.*)
(*Essentially, this means that one can use Toffoli gates to build systems that will perform any desired boolean function calculation in a reversible manner.*)


(* ::Section:: *)
(*Related logic gates*)
